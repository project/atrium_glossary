<?php

/**
 * Implementation of hook_strongarm().
 */
function atrium_glossary_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'atrium_activity_update_type_atrium_glossary_term';
  $strongarm->value = 1;

  $export['atrium_activity_update_type_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_atrium_glossary_term';
  $strongarm->value = 0;

  $export['comment_anonymous_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_atrium_glossary_term';
  $strongarm->value = '0';

  $export['comment_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_controls_atrium_glossary_term';
  $strongarm->value = '3';

  $export['comment_controls_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_atrium_glossary_term';
  $strongarm->value = '4';

  $export['comment_default_mode_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_order_atrium_glossary_term';
  $strongarm->value = '1';

  $export['comment_default_order_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_atrium_glossary_term';
  $strongarm->value = '50';

  $export['comment_default_per_page_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_atrium_glossary_term';
  $strongarm->value = '0';

  $export['comment_form_location_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_atrium_glossary_term';
  $strongarm->value = '1';

  $export['comment_preview_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_atrium_glossary_term';
  $strongarm->value = '1';

  $export['comment_subject_field_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_upload_atrium_glossary_term';
  $strongarm->value = '0';

  $export['comment_upload_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_upload_images_atrium_glossary_term';
  $strongarm->value = 'none';

  $export['comment_upload_images_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_atrium_glossary_term';
  $strongarm->value = array(
    'title' => '-5',
    'body_field' => '-3',
    'revision_information' => '1',
    'author' => '2',
    'options' => '3',
    'comment_settings' => '4',
    'menu' => '-4',
    'book' => '0',
    'og_nodeapi' => '-1',
  );

  $export['content_extra_weights_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_profile_use_atrium_glossary_term';
  $strongarm->value = 0;

  $export['content_profile_use_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'enable_revisions_page_atrium_glossary_term';
  $strongarm->value = 1;

  $export['enable_revisions_page_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'form_build_id_atrium_glossary_term';
  $strongarm->value = 'form-603946d65652951faf3da5ee11851a45';

  $export['form_build_id_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'itweak_upload_collapse_atrium_glossary_term';
  $strongarm->value = '0';

  $export['itweak_upload_collapse_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'itweak_upload_comment_display_atrium_glossary_term';
  $strongarm->value = '2';

  $export['itweak_upload_comment_display_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'itweak_upload_node_display_atrium_glossary_term';
  $strongarm->value = '2';

  $export['itweak_upload_node_display_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'itweak_upload_teaser_display_atrium_glossary_term';
  $strongarm->value = '0';

  $export['itweak_upload_teaser_display_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'itweak_upload_teaser_images_atrium_glossary_term';
  $strongarm->value = '0';

  $export['itweak_upload_teaser_images_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'itweak_upload_thumbnail_link_comment_atrium_glossary_term';
  $strongarm->value = '_default';

  $export['itweak_upload_thumbnail_link_comment_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'itweak_upload_thumbnail_link_default_atrium_glossary_term';
  $strongarm->value = '_default';

  $export['itweak_upload_thumbnail_link_default_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'itweak_upload_thumbnail_link_node_atrium_glossary_term';
  $strongarm->value = '_default';

  $export['itweak_upload_thumbnail_link_node_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'itweak_upload_thumbnail_link_teaser_atrium_glossary_term';
  $strongarm->value = '_default';

  $export['itweak_upload_thumbnail_link_teaser_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'itweak_upload_thumbnail_link_upload_atrium_glossary_term';
  $strongarm->value = '_default';

  $export['itweak_upload_thumbnail_link_upload_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'itweak_upload_thumbnail_preset_comment_atrium_glossary_term';
  $strongarm->value = '_default';

  $export['itweak_upload_thumbnail_preset_comment_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'itweak_upload_thumbnail_preset_default_atrium_glossary_term';
  $strongarm->value = '_default';

  $export['itweak_upload_thumbnail_preset_default_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'itweak_upload_thumbnail_preset_node_atrium_glossary_term';
  $strongarm->value = '_default';

  $export['itweak_upload_thumbnail_preset_node_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'itweak_upload_thumbnail_preset_teaser_atrium_glossary_term';
  $strongarm->value = '_default';

  $export['itweak_upload_thumbnail_preset_teaser_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'itweak_upload_thumbnail_preset_upload_atrium_glossary_term';
  $strongarm->value = '_default';

  $export['itweak_upload_thumbnail_preset_upload_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'itweak_upload_upload_preview_atrium_glossary_term';
  $strongarm->value = 1;

  $export['itweak_upload_upload_preview_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_atrium_glossary_term';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
    2 => 'revision',
  );

  $export['node_options_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nodeformscols_field_placements_atrium_glossary_term_default';
  $strongarm->value = array(
    'title' => array(
      'region' => 'main',
      'weight' => '-5',
      'has_required' => TRUE,
      'title' => 'Term',
    ),
    'body_field' => array(
      'region' => 'main',
      'weight' => '0.008',
      'has_required' => FALSE,
      'title' => NULL,
      'hidden' => 0,
    ),
    'menu' => array(
      'region' => 'right',
      'weight' => '2',
      'has_required' => FALSE,
      'title' => 'Menu settings',
      'collapsed' => 1,
      'hidden' => 0,
    ),
    'revision_information' => array(
      'region' => 'right',
      'weight' => '4',
      'has_required' => FALSE,
      'title' => 'Revision information',
      'collapsed' => 0,
      'hidden' => 0,
    ),
    'comment_settings' => array(
      'region' => 'right',
      'weight' => '6',
      'has_required' => FALSE,
      'title' => 'Comment settings',
      'collapsed' => 1,
      'hidden' => 1,
    ),
    'options' => array(
      'region' => 'right',
      'weight' => '5',
      'has_required' => FALSE,
      'title' => 'Publishing options',
      'collapsed' => 1,
      'hidden' => 0,
    ),
    'author' => array(
      'region' => 'right',
      'weight' => '3',
      'has_required' => FALSE,
      'title' => 'Authoring information',
      'collapsed' => 1,
      'hidden' => 0,
    ),
    'buttons' => array(
      'region' => 'main',
      'weight' => '100',
      'has_required' => FALSE,
      'title' => NULL,
      'hidden' => 0,
    ),
    'book' => array(
      'region' => 'main',
      'weight' => '10',
      'has_required' => FALSE,
      'title' => 'Book outline',
      'collapsed' => 1,
      'hidden' => 0,
    ),
    'field_atrium_glossary_image' => array(
      'region' => 'right',
      'weight' => '1',
      'has_required' => FALSE,
      'title' => 'Images',
      'hidden' => 0,
    ),
    'og_nodeapi' => array(
      'region' => 'right',
      'weight' => '0',
      'has_required' => FALSE,
      'title' => 'Groups',
      'collapsed' => 0,
      'hidden' => 0,
    ),
    'notifications' => array(
      'region' => 'right',
      'weight' => '7',
      'has_required' => FALSE,
      'title' => 'Notifications',
      'collapsed' => 0,
      'hidden' => 0,
    ),
  );

  $export['nodeformscols_field_placements_atrium_glossary_term_default'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_content_type_atrium_glossary_term';
  $strongarm->value = array(
    0 => 'nodetype',
  );

  $export['notifications_content_type_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'notifications_node_ui_atrium_glossary_term';
  $strongarm->value = array();

  $export['notifications_node_ui_atrium_glossary_term'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'upload_atrium_glossary_term';
  $strongarm->value = '0';

  $export['upload_atrium_glossary_term'] = $strongarm;
  return $export;
}
