<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function atrium_glossary_user_default_permissions() {
  $permissions = array();

  // Exported permission: create atrium_glossary_term content
  $permissions['create atrium_glossary_term content'] = array(
    'name' => 'create atrium_glossary_term content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: delete any atrium_glossary_term content
  $permissions['delete any atrium_glossary_term content'] = array(
    'name' => 'delete any atrium_glossary_term content',
    'roles' => array(
      '0' => 'manager',
      '1' => 'site admin',
    ),
  );

  // Exported permission: delete own atrium_glossary_term content
  $permissions['delete own atrium_glossary_term content'] = array(
    'name' => 'delete own atrium_glossary_term content',
    'roles' => array(),
  );

  // Exported permission: edit any atrium_glossary_term content
  $permissions['edit any atrium_glossary_term content'] = array(
    'name' => 'edit any atrium_glossary_term content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  // Exported permission: edit own atrium_glossary_term content
  $permissions['edit own atrium_glossary_term content'] = array(
    'name' => 'edit own atrium_glossary_term content',
    'roles' => array(
      '0' => 'authenticated user',
    ),
  );

  return $permissions;
}
