<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function atrium_glossary_context_default_contexts() {
  $export = array();
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'atrium_glossary';
  $context->description = '';
  $context->tag = 'Atrium Glossary';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'atrium_glossary_term' => 'atrium_glossary_term',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'menu' => 'atrium_glossary',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Atrium Glossary');

  $export['atrium_glossary'] = $context;
  return $export;
}
